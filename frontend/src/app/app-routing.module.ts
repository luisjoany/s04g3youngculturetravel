import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleUserComponent } from './user/detalle-user.component';
import { EditUserComponent } from './user/edit-user.component';
import { ListaUserComponent } from './user/lista-user.component';
import { NuevoUserComponent } from './user/nuevo-user.component';

const routes: Routes = [
  {path: '',component: ListaUserComponent},
  {path: 'detalle/:id', component: DetalleUserComponent},
  {path: 'nuevo', component: NuevoUserComponent},
  {path: 'edit/:id', component: EditUserComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
