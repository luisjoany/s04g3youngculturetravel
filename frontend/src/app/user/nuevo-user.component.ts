import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { User } from '../models/user';
import { ToastrService } from 'ngx-toastr';
import { Router, Routes } from '@angular/router';

@Component({
  selector: 'app-nuevo-user',
  templateUrl: './nuevo-user.component.html',
  styleUrls: ['./nuevo-user.component.css']
})
export class NuevoUserComponent implements OnInit {

  nombre: string = '';
  departamento: string = '';
  municipio: string = '';
  lugar: string = '';
  descripcion: string = '';

  constructor(
    private userService: UserService,
    private Toastr: ToastrService,
    private router: Router
    ) { }

  ngOnInit(){
  }

  onCreate():void{
    const user = new User(this.nombre, this.departamento, this.municipio, this.lugar, this.descripcion);
    this.userService.save(user).subscribe(
      data =>{
        this.Toastr.success('User Creado', 'OK',{
          timeOut: 3000
        });
        this.router.navigate(['/']);
      },
      err =>{
        this.Toastr.error(err.error.mensaje,'Fail',{
          timeOut: 3000
        });
        this.router.navigate(['/']);
      }
    )
  }

}
