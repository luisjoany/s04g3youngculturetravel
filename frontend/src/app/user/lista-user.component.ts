import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { User } from '../models/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-lista-user',
  templateUrl: './lista-user.component.html',
  styleUrls: ['./lista-user.component.css']
})
export class ListaUserComponent implements OnInit {

  usercrud: User[] = [];

  constructor(
    private userService: UserService,
    private toastr: ToastrService
    ) { }

  ngOnInit() {
    this.cargarUser();
  }

  cargarUser(): void{
    this.userService.lista().subscribe(
      data =>{
        this.usercrud = data;
      },
      err =>{
        console.log(err);
      }

    );
  }

  borrar(id?: number){
    this.userService.delete(id).subscribe(
      data => {
        this.toastr.success('Producto Eliminado', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.cargarUser();
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
      }
    );
  }


  
}



