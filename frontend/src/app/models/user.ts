export class User {
    
    id?: number;
    nombre: string;
    departamento: string;
    municipio: string;
    lugar: string;
    descripcion: string;
      
    constructor(nombre: string, departamento: string, municipo: string, lugar: string, descripcion: string){
          this.nombre = nombre;
          this.departamento = departamento;
          this.municipio = municipo;
          this.lugar = lugar;
          this.descripcion = descripcion;
    }
      
    
}
