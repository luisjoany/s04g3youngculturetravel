import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userURL = 'http://localhost:8080/usercrud/';


  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<User[]>{
    return this.httpClient.get<User[]>(this.userURL + 'lista');
  }

  public detail(id: number): Observable<User>{
    return this.httpClient.get<User>(this.userURL + `detail/${id}`);
  }

  public detailName(nombre: string): Observable<User>{
    return this.httpClient.get<User>(this.userURL + `detailname/${nombre}`);
  }

  public save(user: User): Observable<any>{
    return this.httpClient.post<any>(this.userURL + 'create', user);
  }

  public update(id: number, user: User ): Observable<any>{
    return this.httpClient.put<any>(this.userURL + `update/${id}`, user);
  }

  public delete(id?: number): Observable<any>{
    return this.httpClient.delete<any>(this.userURL + `delete/${id}` )
  }

}