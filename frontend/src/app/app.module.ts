import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ListaUserComponent} from './user/lista-user.component';
import {NuevoUserComponent} from './user/nuevo-user.component';
import { EditUserComponent } from './user/edit-user.component';
import { DetalleUserComponent } from './user/detalle-user.component';


import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

//external
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';





@NgModule({
  declarations: [
    AppComponent,
    ListaUserComponent,
    NuevoUserComponent,
    EditUserComponent,
    DetalleUserComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot(),
    HttpClientModule,
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
