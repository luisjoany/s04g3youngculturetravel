package com.youngCultureTravel.app.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.youngCultureTravel.app.entity.Usercrud;
import com.youngCultureTravel.app.repository.UserRepositorycrud;


@Service
@Transactional
public class UserServicecrud {
	
	@Autowired
	UserRepositorycrud userRepositorycrud;
	
	public List<Usercrud> list(){
		return userRepositorycrud.findAll();
		
	}
	
	
	public Optional<Usercrud> getOne(int id){
		return userRepositorycrud.findById(id);
	}
	
	
	
	public Optional<Usercrud> getByNombre(String nombre){
		return userRepositorycrud.findByNombre(nombre);
	}
	
	
	public void save(Usercrud usercrud) {
		userRepositorycrud.save(usercrud);
	}
	
	
	public void delete(int id) {
		userRepositorycrud.deleteById(id);
	}
	
	
	public boolean existsById(int id) {
		return userRepositorycrud.existsById(id);
	}
	
	public boolean existsByNombre(String nombre) {
		return userRepositorycrud.existsByNombre(nombre);
	}
	
	
	
}
