package com.youngCultureTravel.app.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.youngCultureTravel.app.dto.UserMessagecrud;
import com.youngCultureTravel.app.dto.Userdtocrud;
import com.youngCultureTravel.app.entity.Usercrud;
import com.youngCultureTravel.app.service.UserServicecrud;

@RestController
@RequestMapping("/usercrud")
@CrossOrigin(origins = "http://localhost:4200")
public class UserControllercrud {
	
	@Autowired
	UserServicecrud userServicecrud;
	
	@GetMapping("/lista")
	public ResponseEntity<List<Usercrud>> list(){
		List<Usercrud> list = userServicecrud.list();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	
	@GetMapping("/detail/{id}")
	public ResponseEntity<Usercrud> getById(@PathVariable("id") int id){
		if(!userServicecrud.existsById(id))
			return new ResponseEntity(new UserMessagecrud("no existe"), HttpStatus.NOT_FOUND);
		Usercrud usercrud = userServicecrud.getOne(id).get(); 
		return new ResponseEntity(usercrud, HttpStatus.OK);
	}
	
	@GetMapping("/detailname/{nombre}")
	public ResponseEntity<Usercrud> getByNombre(@PathVariable("nombre") String nombre){
		if(!userServicecrud.existsByNombre(nombre)) 
			return new ResponseEntity(new UserMessagecrud("no existe"), HttpStatus.NOT_FOUND);
		Usercrud usercrud = userServicecrud.getByNombre(nombre).get(); 
		return new ResponseEntity(usercrud, HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> create(@RequestBody Userdtocrud userdtocrud){
		if(StringUtils.isBlank(userdtocrud.getNombre()))
			return new ResponseEntity(new UserMessagecrud("El nombre es obligatorio"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(userdtocrud.getDepartamento()))
			return new ResponseEntity(new UserMessagecrud("El Departamento es obligatorio"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(userdtocrud.getMunicipio()))
			return new ResponseEntity(new UserMessagecrud("El Municipio es obligatorio"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(userdtocrud.getLugar()))
			return new ResponseEntity(new UserMessagecrud("El Lugar es obligatorio"), HttpStatus.BAD_REQUEST);
		Usercrud usercrud = new Usercrud(userdtocrud.getNombre(), userdtocrud.getDepartamento(),userdtocrud.getMunicipio(), userdtocrud.getLugar(), userdtocrud.getDescripcion());
		userServicecrud.save(usercrud);
		return new ResponseEntity(new UserMessagecrud("Lugar favorito Creado con exito"), HttpStatus.OK);	
		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> update(@PathVariable("id") int id, @RequestBody Userdtocrud userdtocrud){
		if(!userServicecrud.existsById(id))
			return new ResponseEntity(new UserMessagecrud("no existe"), HttpStatus.NOT_FOUND);
		if(userServicecrud.existsByNombre(userdtocrud.getNombre())&& userServicecrud.getByNombre(userdtocrud.getNombre()).get().getId() != id)
			return new ResponseEntity(new UserMessagecrud("El nombre ya existe"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(userdtocrud.getNombre()))
			return new ResponseEntity(new UserMessagecrud("El nombre es obligatorio"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(userdtocrud.getDepartamento()))
			return new ResponseEntity(new UserMessagecrud("El Departamento es obligatorio"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(userdtocrud.getMunicipio()))
			return new ResponseEntity(new UserMessagecrud("El Municipio es obligatorio"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(userdtocrud.getLugar()))
			return new ResponseEntity(new UserMessagecrud("El Lugar es obligatorio"), HttpStatus.BAD_REQUEST);
		Usercrud usercrud = userServicecrud.getOne(id).get();
		usercrud.setNombre(userdtocrud.getNombre());
		usercrud.setDepartamento(userdtocrud.getDepartamento());
		usercrud.setMunicipio(userdtocrud.getMunicipio());
		usercrud.setLugar(userdtocrud.getLugar());
		usercrud.setDescripcion(userdtocrud.getDescripcion());
		userServicecrud.save(usercrud);
		return new ResponseEntity(new UserMessagecrud("Lugar favorito Actualizado con exito"), HttpStatus.OK);	
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id")int id){
		if(!userServicecrud.existsById(id))
			return new ResponseEntity(new UserMessagecrud("no existe"), HttpStatus.NOT_FOUND);
		userServicecrud.delete(id);
		return new ResponseEntity(new UserMessagecrud("Lugar Eliminado -_-"), HttpStatus.OK);
	}
	
}
