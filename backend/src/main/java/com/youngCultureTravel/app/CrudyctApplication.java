package com.youngCultureTravel.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudyctApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudyctApplication.class, args);
	}

}
