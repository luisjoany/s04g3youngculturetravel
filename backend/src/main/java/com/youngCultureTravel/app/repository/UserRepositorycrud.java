package com.youngCultureTravel.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.youngCultureTravel.app.entity.Usercrud;

@Repository
public interface UserRepositorycrud extends JpaRepository<Usercrud, Integer>{

	Optional<Usercrud> findByNombre(String nombre);
	boolean existsByNombre(String nombre);
	
}
