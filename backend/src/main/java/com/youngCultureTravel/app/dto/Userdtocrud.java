package com.youngCultureTravel.app.dto;

import javax.validation.constraints.NotBlank;;

public class Userdtocrud {
	
	@NotBlank
	private String nombre;
	@NotBlank
	private String departamento;
	@NotBlank
	private String municipio;
	@NotBlank
	private String lugar;

	private String descripcion;
	
	
	
	public Userdtocrud() {
	}



	public Userdtocrud(String nombre, String departamento, String municipio, String lugar, String descripcion) {
		this.nombre = nombre;
		this.departamento = departamento;
		this.municipio = municipio;
		this.lugar = lugar;
		this.descripcion = descripcion;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getDepartamento() {
		return departamento;
	}



	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}



	public String getMunicipio() {
		return municipio;
	}



	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}



	public String getLugar() {
		return lugar;
	}



	public void setLugar(String lugar) {
		this.lugar = lugar;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

	
	
	
	
	
}
