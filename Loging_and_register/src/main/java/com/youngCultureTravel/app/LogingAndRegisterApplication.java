package com.youngCultureTravel.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogingAndRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogingAndRegisterApplication.class, args);
	}

}
